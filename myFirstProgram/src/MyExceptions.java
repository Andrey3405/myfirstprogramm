/**
 * Created by Пользователь on 20.08.2014.
 */
public final class MyExceptions {
    public final static class NumberNegativeException extends RuntimeException {
        public NumberNegativeException() {
            super("Отрицательное число!");
        }
    }

    public final static class StringIsNullOrEmpty extends RuntimeException {
        public StringIsNullOrEmpty() {
            super("Строка является пустой или не существует!");
        }
    }

    public final static class ArrayIsEmptyException extends RuntimeException {
        public ArrayIsEmptyException() {
            super("Данный массив не имеет записей!");
        }
    }

    public final static class ArgumentNullException extends RuntimeException{
        public ArgumentNullException(){ super("Объект является null");}
    }
}

