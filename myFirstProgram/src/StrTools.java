/**
 * Created by Пользователь on 20.08.2014.
 */
public final class StrTools {
        //public static final String Empty="";
        public static boolean IsNullOrEmpty(String str){
            return (str == null) || (str.trim().equals(""));
        }

        public static boolean Equals(String first, String second) {
            return first.equals(second);
        }
}
