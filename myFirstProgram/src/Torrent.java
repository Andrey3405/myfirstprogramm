import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Пользователь on 22.09.2014.
 */
public final class Torrent {
    public ArrayList<BencodeParameter> TorrentList;
    final static String[] _size_types = new String[]{"B", "KB", "MB", "GB", "TB", "PB"};

    public Torrent(ArrayList<BencodeParameter> arrayList) {
        if (arrayList == null || arrayList.size() == 0) {
            throw new MyExceptions.ArrayIsEmptyException();
        }
        TorrentList = new ArrayList<BencodeParameter>();
        TorrentList.addAll(arrayList);
    }

    //Вспомогательный метод для поиска параметра в листе
    Object GetParameter(ArrayList<BencodeParameter> arrayList, String seachParameter) {
        if (StrTools.IsNullOrEmpty(seachParameter)) {
            throw new MyExceptions.StringIsNullOrEmpty();
        }
        if (arrayList == null) {
            throw new MyExceptions.ArgumentNullException();
        }
        if (arrayList.size() == 0) {
            throw new MyExceptions.ArrayIsEmptyException();
        }
        for (int i = 0; i < arrayList.size(); i++) {
            if (StrTools.Equals(arrayList.get(i).NameParameter, seachParameter)) {
                return arrayList.get(i).ValueParameter;
            }
        }
        return null;
    }

    //Поиск announce
    public String GetAnnounce() {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        return (String) GetParameter(TorrentList, "announce");
    }

    //Поиск комментария
    public String GetComment() {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        return (String) GetParameter(TorrentList, "comment");
    }

    //Поиск создателя
    public String GetCreatedBy() {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        return (String) GetParameter(TorrentList, "created by");
    }

    //Поиск кодировки
    public String GetEncoding() {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        return (String) GetParameter(TorrentList, "encoding");
    }

    //Поиск даты создания
    public Integer GetCreationDate() {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        Object obj = GetParameter(TorrentList, "creation date");
        if (obj != null) {
            return Integer.parseInt(obj.toString());
        }
        return null;
    }

    //Поиск опубликовавшего
    public String GetPublisher() {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        return (String) GetParameter(TorrentList, "publisher");
    }

    //Поиск url опубликовавшего
    public String GetPublisherUrl() {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        return (String) GetParameter(TorrentList, "publisher-url");
    }

    //Вспомогательный метод для поиска в списках
    private String GetDownString(ArrayList<BencodeParameter> arrayList) {
        if (TorrentList.get(0).TypeParameter == BencodeParameter.TypeParameterEnum.ArrayList) {
            return GetDownString((ArrayList<BencodeParameter>) TorrentList.get(0).ValueParameter);
        }
        return (String) TorrentList.get(0).ValueParameter;
    }

    //Поиск списка announce-list
    public ArrayList<String> GetAnnounceList() {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        ArrayList<BencodeParameter> temp = (ArrayList<BencodeParameter>) GetParameter(TorrentList, "announce-list");
        ArrayList<String> arrayStringList = new ArrayList<String>();
        for (int i = 0; i < temp.size(); i++) {
            arrayStringList.add(GetDownString((ArrayList<BencodeParameter>) temp.get(i).ValueParameter));
        }
        return arrayStringList;
    }

    public ArrayList<BencodeParameter> GetInfo(ArrayList<BencodeParameter> arrayList) {
        if (TorrentList == null){
            throw new MyExceptions.ArrayIsEmptyException();
        }
        return (ArrayList<BencodeParameter>) GetParameter(arrayList, "info");
    }

    //Переводим байты в более большую единицу (для "piece length")
    public String GetSize(long size) {
        if (size < 0) {
            throw new MyExceptions.NumberNegativeException();
        }
        float result = (float) size;
        int i = 0;
        while (result >= 1024) {
            result /= 1024;
            i += 1;
        }
        return String.format("%.2f %s", result, _size_types[i]);
    }

    //Преобразуем дату добавляя секунды к дате 1970.01.01
    public Date GetDateConverted(int seconds) {
        if (seconds < 0) {
            throw new MyExceptions.NumberNegativeException();
        }
        Calendar result = new GregorianCalendar(1970, 1, 1);
        result.add(Calendar.SECOND, seconds);
        return result.getTime();
    }

    public void SeachThreeElements(ArrayList<BencodeParameter> obj) {
        for (BencodeParameter anObj : obj) {
            if (anObj.TypeParameter.equals("ArrayList<TorrentParameter>")) {
                SeachThreeElements((ArrayList<BencodeParameter>) anObj.ValueParameter);
                continue;
            }
            System.out.println(anObj.NameParameter + " " + anObj.ValueParameter);
        }
    }

    //Вывод всего дерева ключевых слов торрент файла
    public void ToStringArray() {
        if(TorrentList == null) {
            throw new MyExceptions.ArgumentNullException();
        }
        SeachThreeElements(TorrentList);
    }

    public boolean TorrentListIsNull(){
        if(TorrentList == null) return true;
        else return false;
    }

    public int TorrentSize(){
        if(TorrentList == null) {
            throw new MyExceptions.ArgumentNullException();
        }
        return TorrentList.size();
    }

}
