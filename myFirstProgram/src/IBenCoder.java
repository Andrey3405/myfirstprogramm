import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Пользователь on 22.09.2014.
 */
public interface IBenCoder {
    final class NumberSymbols {
        public int n = 0;
    }
    ArrayList<BencodeParameter> Parsing(String filename);
    String Coder(ArrayList<BencodeParameter> arrayList, boolean start);
}
