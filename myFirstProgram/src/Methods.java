import java.io.*;
import java.nio.charset.Charset;

public final class Methods {

    //Выделяем из текста числовое значение указывающее длину далее идущего параметра
    public static String GetCount(int startChar, String text) {
        if (startChar < 0 || startChar > text.length()) {
            throw new MyExceptions.NumberNegativeException();
        }
        if (StrTools.IsNullOrEmpty(text)) {
            throw new MyExceptions.StringIsNullOrEmpty();
        }
        String result = "";
        for (int i = startChar; Character.isDigit(text.charAt(i)); i++) {
            result += text.charAt(i);
        }
        return result;
    }

    //Чтение файла с указание кодировки
    public static String ReadFile(String filename, Charset charsetName) {
        if(charsetName == null){
            throw  new NullPointerException("Charset не указан!");
        }
        if(StrTools.IsNullOrEmpty(filename)){
            throw  new MyExceptions.StringIsNullOrEmpty();
        }
        String result="";
        try{
            InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(filename),charsetName);
            int a;
            while((a=inputStreamReader.read())!=-1){
                    result+=(char)a;
            }
            inputStreamReader.close();
        }
        catch(IOException e) {
            System.out.println("Error - " + e.toString());
        }
        return result;
    }
}
