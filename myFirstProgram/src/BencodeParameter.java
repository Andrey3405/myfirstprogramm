/**
 * Created by Home on 15.10.2014.
 */

public class BencodeParameter {
    public enum TypeParameterEnum{
        Integer,
        String,
        ArrayList,
        Dictionary,
    }

    public String NameParameter;
    public Integer LengthValue;
    public TypeParameterEnum TypeParameter;
    public Object ValueParameter;

    public BencodeParameter (String nameParameter, TypeParameterEnum typeParameter, Integer lengthValue, Object valueParameter){
        this.NameParameter = nameParameter;
        this.TypeParameter = typeParameter;
        this.LengthValue = lengthValue;
        this.ValueParameter = valueParameter;
    }

    public BencodeParameter(){
        this("",null,0,null);
    }



    public String toString(){
        return  "Параметр: "+this.NameParameter+"\n"+
                "Тип: "+this.TypeParameter+"\n"+
                "Длина: "+this.LengthValue+"\n"+
                "Значение: "+this.ValueParameter;

    }
}
