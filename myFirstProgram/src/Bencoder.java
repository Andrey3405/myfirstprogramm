import java.util.ArrayList;

/**
 * Created by Пользователь on 22.09.2014.
 */
public final class  Bencoder implements IBenCoder {

    private BencodeParameter.TypeParameterEnum typeParameterEnum;

    private void GetInteger(String file, NumberSymbols numberChar, BencodeParameter result, ArrayList<BencodeParameter> arrayList) {
        result.ValueParameter = Methods.GetCount(++numberChar.n, file);
        result.TypeParameter = typeParameterEnum.Integer;
        result.LengthValue = -1;
        numberChar.n += ((String) result.ValueParameter).length() + 1;
        arrayList.add(result);
    }

    private void GetString(String file, NumberSymbols numberChar, BencodeParameter result, String length, ArrayList<BencodeParameter> arrayList) {
        result.ValueParameter = file.substring(numberChar.n, numberChar.n + Integer.parseInt(length));
        result.TypeParameter = typeParameterEnum.String;
        result.LengthValue = Integer.parseInt(length);
        arrayList.add(result);
    }

    private void GetListOrDictionary(String file, NumberSymbols numberSymbols, BencodeParameter result, ArrayList<BencodeParameter> arrayList, boolean isNameOrValue) {
        result.ValueParameter = ParsingTorrentFile(file, numberSymbols, isNameOrValue);
        result.LengthValue = ((ArrayList<BencodeParameter>) result.ValueParameter).size();
        result.TypeParameter = (isNameOrValue) ? typeParameterEnum.ArrayList : typeParameterEnum.Dictionary;
        arrayList.add(result);
    }

    //Разложение торрент файла по ключевым словам
    private ArrayList<BencodeParameter> ParsingTorrentFile(final String file, NumberSymbols numberSymbol, boolean isNameOrValue) {
        if (StrTools.IsNullOrEmpty(file)) {
            throw new MyExceptions.StringIsNullOrEmpty();
        }
        if (numberSymbol.n < 0) {
            throw new MyExceptions.NumberNegativeException();
        }
        if (numberSymbol.n > file.length()) {
            throw new MyExceptions.ArgumentNullException();
        }
        BencodeParameter parameter = new BencodeParameter();
        ArrayList<BencodeParameter> result = new ArrayList<BencodeParameter>();
        String digit = "0";
        while (numberSymbol.n < file.length()) {
            if (file.charAt(numberSymbol.n) == 'e') {
                numberSymbol.n++;
                break;
            }
            if (file.charAt(numberSymbol.n) == 'i') {
                isNameOrValue = false;
                GetInteger(file, numberSymbol, parameter, result);
                parameter = new BencodeParameter();
                continue;
            }
            if (Character.isDigit(file.charAt(numberSymbol.n))) {
                digit = Methods.GetCount(numberSymbol.n, file);
                numberSymbol.n += digit.length() + 1;
                if (!isNameOrValue) {
                    isNameOrValue = true;
                    parameter.NameParameter = file.substring(numberSymbol.n, numberSymbol.n + Integer.parseInt(digit));
                } else {
                    isNameOrValue =false;
                    GetString(file, numberSymbol, parameter, digit, result);
                    parameter = new BencodeParameter();
                }
                numberSymbol.n += Integer.parseInt(digit);
                continue;
            }
            if (file.charAt(numberSymbol.n) == 'l') {
                numberSymbol.n++;
                isNameOrValue = false;
                GetListOrDictionary(file, numberSymbol, parameter, result, true);
                parameter = new BencodeParameter();
                continue;
            }
            if (file.charAt(numberSymbol.n) == 'd') {
                if (++numberSymbol.n == 1) {
                    result = new ArrayList<BencodeParameter>();
                } else {
                    isNameOrValue = false;
                    GetListOrDictionary(file, numberSymbol, parameter, result, false);
                    parameter = new BencodeParameter();
                }
            }
        }
        return result;
    }

    //Чтение торрент файла
    public ArrayList<BencodeParameter> Parsing(String torrentContent) {
        if (StrTools.IsNullOrEmpty(torrentContent)) {
            throw new MyExceptions.StringIsNullOrEmpty();
        }
        return ParsingTorrentFile(torrentContent, new NumberSymbols(), false);
    }

    public String Coder(ArrayList<BencodeParameter> arrayList, boolean start){
        if(arrayList == null) {
            throw new MyExceptions.ArgumentNullException();
        }
        if(arrayList.size()==0) {
            throw new MyExceptions.ArrayIsEmptyException();
        }
        String result=(start) ? "d" : "";
        BencodeParameter bencodeParameter;
        for(int i =0;i<arrayList.size();i++) {
            bencodeParameter = arrayList.get(i);

            if(bencodeParameter.NameParameter.length()!=0) {
                result += bencodeParameter.NameParameter.length() + ":";
            }
            switch(bencodeParameter.TypeParameter) {
                case Integer:
                    result += bencodeParameter.NameParameter + "i" + bencodeParameter.ValueParameter+"e";
                    break;
                case ArrayList:
                    result +=bencodeParameter.NameParameter +"l"+Coder(((ArrayList<BencodeParameter>) bencodeParameter.ValueParameter), false);
                    break;
                case String:
                    result += bencodeParameter.NameParameter+bencodeParameter.LengthValue+":" + bencodeParameter.ValueParameter;
                    break;
                case Dictionary:
                    result +=bencodeParameter.NameParameter+Coder(((ArrayList<BencodeParameter>)bencodeParameter.ValueParameter),true);
                    break;
            }
        }
        return result+"e";
    }
}
